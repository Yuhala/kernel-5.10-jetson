#!/bin/bash

#
# Bash script to copy modified kernel modules for Jetson AGX Xavier
# Copyright 2023 Peterson Yuhala
# 


MODIFIED_MODULES="./modified_modules/*.ko"
KERNEL_VERSION="5.10.104-tegra"

MODULES_DIR="/usr/lib/modules/"
#TEGRA_SOUND_MODULES="kernel/sound/soc/tegra"
TEGRA_SOUND_MODULES="kernel/sound/soc/tegra"

sudo cp $MODIFIED_MODULES $MODULES_DIR/$KERNEL_VERSION/$TEGRA_SOUND_MODULES
echo "===== replaced modified modules ====="
