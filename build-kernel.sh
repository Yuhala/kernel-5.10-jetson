#!/bin/bash

#
# Script to build Linux kernel for Jetson AGX Xavier
# Copyright Peterson Yuhala
# 

# 
# NB: do not run this file
# Still work in progress
#

KERNEL_OUT=./kernel_out
MODULES_OUT=./modules_out

CC_AARCH64_PATH=/home/ubuntu/aarch64--glibc--stable-2020.08-1
CC_AARCH64=/home/ubuntu/aarch64--glibc--stable-final/bin/aarch64-buildroot-linux-gnu-


mkdir -p $KERNEL_OUT
mkdir -p $MODULES_OUT

export CROSS_COMPILE_AARCH64_PATH=$CC_AARCH64_PATH
export CROSS_COMPILE_AARCH64=$CC_AARCH64

# Compile/Build kernel
sh nvbuild.sh -o $KERNEL_OUT

# Install the kernel modules
# make -C ./kernel/kernel-5.10/ ARCH=arm64 O=$KERNEL_OUT LOCALVERSION=-tegra INSTALL_MOD_PATH=$MODULES_OUT modules_install -j4

make -C $PWD/kernel/kernel-5.10/ ARCH=arm64 O=$PWD/kernel_out LOCALVERSION=-tegra INSTALL_MOD_PATH=$PWD/modules_out modules_install -j4