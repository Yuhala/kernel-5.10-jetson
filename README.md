
### Building Jetson Linux Kernel sources
- To manually build the kernel, see: https://docs.nvidia.com/jetson/archives/r34.1/DeveloperGuide/text/SD/Kernel/KernelCustomization.html#to-manually-download-and-expand-the-kernel-sources
- Install kernel build utilities
```
 sudo apt install build-essential bc
```
- If you are cross-compiling on a host system (i.e. anything other than the Jetson device you are building for), download the cross-compilation toolchain:
```
wget https://toolchains.bootlin.com/downloads/releases/toolchains/aarch64/tarballs/aarch64--glibc--stable-2020.08-1.tar.bz2
tar -xf aarch64--glibc--stable-2020.08-1.tar.bz2 
export CROSS_COMPILE_AARCH64_PATH=/home/ubuntu/aarch64--glibc--stable-2020.08-1
```
- Create directory where compiled kernel and moduels will be written to:
```
mkdir kernel_out
mkdir modules_out
```
- Build the kernel
```
./nvbuild.sh -o $PWD/kernel_out
```
- Replace Linux_for_Tegra/rootfs/usr/lib/modules/$(uname -r)/kernel/drivers/gpu/nvgpu/nvgpu.ko with a copy of this file:
```
kernel_out/drivers/gpu/nvgpu/nvgpu.ko
```
- Replace Linux_for_Tegra/kernel/Image with a copy of this file:
```
kernel_out/arch/arm64/boot/Image
```
- Install the kernel modules
```
make -C $PWD/kernel/kernel-5.10/ ARCH=arm64 O=$PWD/kernel_out LOCALVERSION=-tegra INSTALL_MOD_PATH=$PWD/modules_out modules_install -j4
```
```
sudo make ARCH=arm64 O=$TEGRA_KERNEL_OUT modules_install \
    INSTALL_MOD_PATH=<top>/Linux_for_Tegra/rootfs/
```

- Optionally, archive the installed kernel modules:
```
cd <modules_install_path>
tar --owner root --group root -cjf kernel_supplements.tbz2 lib/modules
```

## Re-flash the kit with the updated Kernel
- See: https://gitlab.com/Yuhala/jetson-xavier/-/tree/main#setting-up-the-jetson-agx-xavier
```
cd Linux_for_Tegra/
sudo ./apply_binaries.sh
sudo ./tools/l4t_flash_prerequisites.sh
sudo ./flash.sh jetson-agx-xavier-devkit internal

```

## Access tokens
Access token: glpat-_xNWn17zVJ42hX3fDbe7

## References
- https://docs.nvidia.com/jetson/archives/l4t-archived/l4t-325/index.html#page/Tegra%20Linux%20Driver%20Package%20Development%20Guide/kernel_custom.html#
- Jetson Kernel Customization: https://docs.nvidia.com/jetson/archives/r34.1/DeveloperGuide/text/SD/Kernel/KernelCustomization.html#to-build-the-kernel
- Jetson Orin Kernel build: https://developer.ridgerun.com/wiki/index.php/NVIDIA_Jetson_Orin/JetPack_5.0.2/Compiling_Code
