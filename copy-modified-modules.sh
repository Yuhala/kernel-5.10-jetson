#!/bin/bash

#
# Bash script to copy modified kernel modules for Jetson AGX Xavier
# Copyright 2023 Peterson Yuhala
# 

MODULES_OUT="./modules_out"
MODIFIED_MODULES="./modified_modules"
KERNEL_VERSION="5.10.104-tegra"

TEGRA_SOUND_MODULES=$MODULES_OUT/lib/modules/$KERNEL_VERSION/kernel/sound/soc/tegra

cp $TEGRA_SOUND_MODULES/snd-soc-tegra210-i2s.ko $MODIFIED_MODULES
echo "copied: snd-soc-tegra210-i2s.ko"
cp $TEGRA_SOUND_MODULES/snd-soc-tegra210-admaif.ko $MODIFIED_MODULES
echo "copied: snd-soc-tegra210-admaif.ko"
cp $TEGRA_SOUND_MODULES/snd-soc-tegra210-adsp.ko $MODIFIED_MODULES
echo "copied: snd-soc-tegra210-adsp.ko"
cp $TEGRA_SOUND_MODULES/snd-soc-tegra210-ahub.ko $MODIFIED_MODULES
echo "copied: snd-soc-tegra210-ahub.ko"
cp $TEGRA_SOUND_MODULES/snd-soc-tegra210-amx.ko $MODIFIED_MODULES
echo "copied: snd-soc-tegra210-amx.ko"
cp $TEGRA_SOUND_MODULES/snd-soc-tegra210-dmic.ko $MODIFIED_MODULES
echo "copied: snd-soc-tegra210-dmic.ko"
cp $TEGRA_SOUND_MODULES/snd-soc-tegra210-mixer.ko $MODIFIED_MODULES
echo "copied: snd-soc-tegra210-mixer.ko"